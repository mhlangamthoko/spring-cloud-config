# ghc-config

Config for our Spring applications

### Guide to adding config



__Required directory structure:__

```
-- project-working-directory
   -- {application-name}-directory
        -- application-{profile}.yml
```

__Definitions:__

- {application-name} = spring.application.name property found in your default config for the project
- {profile} = your assigned profile name, main application config needs to specify the active profile 